#!/bin/bash

function addTvSeries() {

	while [ true ] ; do
	read -p "Name to your TV series  : " tv_name
	read -p "Give the folder path it : " base_dir

	#echo "$tv_name%$base_dir"
	
	file_type="\.(avi|mp4|mkv|mp3)"
	no_of_episodes=$(ls "$base_dir" | grep -Ec "$file_type")
	echo -e "$tv_name\t$base_dir\t$no_of_episodes\t0\t" >> play-v1.conf
	
	read -p "Wanna add more? (yes/no) " con

	regex="^[nN].*"
	
	if [[ "$con" =~ $regex ]]
	then
		break
	else
		continue
	fi
	done

}



function playTv(){

	echo "Getting ready to play \"$1\" ..."
		
	episode_counter=$(grep -wc "$1" play-v1.conf)
	
	if [ $episode_counter -eq 0 ]
	then
		echo "Incorrect name"	
	else
		
		tv_name=$(cat play-v1.conf | grep -w "$1" | cut -f 1)
		base_dir_path=$(cat play-v1.conf | grep -w "$1" | cut -f 2)
		episodes_count=$(cat play-v1.conf | grep -w "$1" | cut -f 3)
		last_played_episode=$(cat play-v1.conf | grep -w "$1" | cut -f 4)

		episodes_to_watch=$(( $episodes_count - $last_played_episode))
		
		if [ $episodes_to_watch -le 0 ]
		then
			echo "you have watched the full season"
		else
			next_episode=$(( $last_played_episode +1 ))			
			file_type="\.(avi|mp4|mkv|mp3)"
			next_episode_name=$(ls "$base_dir_path" | grep -E $file_type| sort -n | sed -n "$next_episode p")
			echo "Episode $next_episode_name is playing now ..."
			
			T_last_episode_details=$(cat play-v1.conf | grep -w T )
			T_tv_name=$(echo "$T_last_episode_details" | cut -f 1)
			T_base_dir=$(echo "$T_last_episode_details" | cut -f 2)
			T_episodes_count=$(echo "$T_last_episode_details" | cut -f 3)
			T_last_episode=$(echo "$T_last_episode_details" | cut -f 4)

	
			sed -i.back "s@$T_tv_name\t.*\$@$T_tv_name\t$T_base_dir\t$T_episodes_count\t$T_last_episode\t@" play-v1.conf
			sed -i.back "s@$tv_name\t.*\$@$tv_name\t$base_dir_path\t$episodes_count\t$next_episode\tT@" play-v1.conf

			vlc "$base_dir_path/$next_episode_name"
		fi
	fi

	

}



conf_file_line_count=$(cat play-v1.conf | wc -l)

if [ $conf_file_line_count -eq 1 ]
then
	addTvSeries

elif [ "$1" == "-add" ]
then
	addTvSeries

elif [ "$1" == "-select" ]
then
	sed -n '2,$ p' play-v1.conf | cut -f 1
	read -p "type the selected name : " selected_name
	
	echo "$selected_name is selected to play"
	playTv "$selected_name"

else
	T_count=$(cat play-v1.conf | grep -w T | wc -l)
	if [ $T_count -eq 0 ]
	then

		first_line=$(sed  -n "2 p" play-v1.conf)		
		tv_name=$(echo "$first_line" | cut -f 1)
		base_dir_path=$(echo "$first_line" |cut -f 2)
		episodes_count=$(echo "$first_line" | cut -f 3)
		last_played_episode=$(echo "$first_line" | cut -f 4)
		
		sed -i.back "s@$tv_name\t.*\$@$tv_name\t$base_dir_path\t$episodes_count\t$last_played_episode\tT@" play-v1.conf

		sed -n '2,$ p' play-v1.conf | cut -f 1
		read -p "type the selected name : " selected_name
		echo "$selected_name is selected to play"
		playTv "$selected_name"
		
	else

		T_last_episode_details=$(cat play-v1.conf | grep -w T )
		T_tv_name=$(echo "$T_last_episode_details" | cut -f 1)
		T_base_dir=$(echo "$T_last_episode_details" | cut -f 2)
		T_episodes_count=$(echo "$T_last_episode_details" | cut -f 3)
		T_last_episode=$(echo "$T_last_episode_details" | cut -f 4)

		next_episode=$(( $T_last_episode +1 ))			
		file_type="\.(avi|mp4|mkv|mp3)"
		next_episode_name=$(ls "$T_base_dir" | grep -E $file_type| sort -n | sed -n "$next_episode p")
	

		echo "$next_episode_name"
		echo "Episode $next_episode_name is playing now ..."

	
		sed -i.back "s@$T_tv_name\t.*\tT\$@$T_tv_name\t$T_base_dir\t$T_episodes_count\t$next_episode\tT@" play-v1.conf


		vlc "$T_base_dir/$next_episode_name"
	fi

fi















